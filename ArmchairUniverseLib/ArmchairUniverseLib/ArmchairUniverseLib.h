//
//  ArmchairUniverseLib.h
//  ArmchairUniverseLib
//
//  Created by Jeremy Pereira on 31/08/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for ArmchairUniverseLib.
FOUNDATION_EXPORT double ArmchairUniverseLibVersionNumber;

//! Project version string for ArmchairUniverseLib.
FOUNDATION_EXPORT const unsigned char ArmchairUniverseLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ArmchairUniverseLib/PublicHeader.h>


