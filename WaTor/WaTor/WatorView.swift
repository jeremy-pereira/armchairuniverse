//
//  WatorView.swift
//  
//
//  Created by Jeremy Pereira on 03/09/2019.
//

import Cocoa

protocol WatorViewDataSource
{

	/// Get the size of the world as a vector
	///
	/// - Parameter view: The view asking for the world size
	/// - Returns: The size of the world
	func worldSize(for view: WatorView) -> World.Vector

	/// Get the animal, if any at a given point
	///
	/// - Parameters:
	///   - forView: The view requesting the animal
	///   - point: The point of the animal we need
	/// - Returns: The animal if there is one at the point
	func animal(forView: WatorView, at point: World.Point) -> Animal?
}


class WatorView: NSView
{

	let cellSize = NSSize(width: 4, height: 4)

	var widthConstraint: NSLayoutConstraint?
	var heightConstraint: NSLayoutConstraint?

	var dataSource: WatorViewDataSource?
	{
		didSet
		{
			if let ds = dataSource
			{
				let size = ds.worldSize(for: self)
				let width = CGFloat(size.x) * cellSize.width
				let height = CGFloat(size.y) * cellSize.height
				self.needsDisplay = true
				if let widthConstraint = widthConstraint
				{
					widthConstraint.isActive = false
				}
				if let heightConstraint = heightConstraint
				{
					heightConstraint.isActive = false
				}
				widthConstraint = self.widthAnchor.constraint(equalToConstant: width)
				widthConstraint?.isActive = true
				heightConstraint = self.heightAnchor.constraint(equalToConstant: height)
				heightConstraint?.isActive = true
			}
		}
	}

    override func draw(_ dirtyRect: NSRect)
	{
        super.draw(dirtyRect)

		NSColor.black.set()
		NSBezierPath.fill(dirtyRect)
		if let dataSource = dataSource
		{
			let worldSize = dataSource.worldSize(for: self)
			for y in 0 ..< worldSize.y
			{
				for x in 0 ..< worldSize.x
				{
					if let animal = dataSource.animal(forView: self, at: World.Point(x, y))
					{
						let rect = NSRect(x: CGFloat(x) * cellSize.width, y: CGFloat(y) * cellSize.height, width: cellSize.width, height: cellSize.height)
						if animal is Shark
						{
							NSColor.red.set()
						}
						else
						{
							NSColor.blue.set()
						}
						NSBezierPath.fill(rect)
					}
				}
			}
		}
    }
    
}
