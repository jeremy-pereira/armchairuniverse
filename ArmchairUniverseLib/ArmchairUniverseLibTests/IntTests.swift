//
//  IntTests.swift
//  ArmchairUniverseLibTests
//
//  Created by Jeremy Pereira on 02/09/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
import ArmchairUniverseLib

class IntTests: XCTestCase
{
    func testModulus()
	{
		XCTAssert((-10).modulus(-3) == 2)
		XCTAssert(10.modulus(3) == 1)
		XCTAssert((-10).modulus(3) == 2)
		XCTAssert(10.modulus(-3) == 1)
		XCTAssert(9.modulus(3) == 0)
		XCTAssert((-9).modulus(3) == 0)
		XCTAssert((-9).modulus(-3) == 0)
		XCTAssert(9.modulus(-3) == 0)
    }
}
