//
//  Random.swift
//  ArmchairUniverseLib
//
//  Created by Jeremy Pereira on 31/08/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Type erased random number generator
///
/// This is needed because some RNG functions require a concrete type that
/// conforms to `RandfomNumberGenerator` and protocols can't conform to
/// themselves in a generic function.
public struct AnyRNG: RandomNumberGenerator
{
	private var realRNG: RandomNumberGenerator

	public init(wrapped: RandomNumberGenerator)
	{
		realRNG = wrapped
	}

	/// Returns a uniform random number.
	///
	/// The quality of the random number generated and the threrad safety is
	/// defined by the underlying random number generator.
	/// - Returns: A random value.
	public mutating func next() -> UInt64
	{
		return realRNG.next()
	}

	/// Wraps a random number generator as an `AnyRNG`
	///
	/// If the rng is already an `AnyRNG` you just get it back.
	///
	/// - Parameter rng: The random number generator to wrap
	/// - Returns: The wrapped random number generator
	public static func wrap(_ rng: RandomNumberGenerator) -> AnyRNG
	{
		return rng as? AnyRNG ?? AnyRNG(wrapped: rng)
	}
}


