//
//  Animal.swift
//  WaTor
//
//  Created by Jeremy Pereira on 31/08/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
import ArmchairUniverseLib

/// An inhabitant of the World of Wator.
class Animal
{
	fileprivate let breedTime: Int
	private var lastBreedAge = 0
	fileprivate private(set) var age: Int


	/// Initialise an animal
	///
	/// - Parameters:
	///   - breed: How long it takes to breed
	///   - age: How old is it at birth
	init(breed: Int, age: Int)
	{
		breedTime = breed
		self.age = age
	}

	/// Make the animal older by one chronon
	func increaseAge()
	{
		age += 1
	}


	/// Try breeeding.
	///
	/// Animal will only breed if it is old enough.
	/// - Parameter rng: Random number generator used to vary the age of
	///                  the offspring
	/// - Returns: A cloned offspring
	func tryBreeding(rng: inout AnyRNG) -> Animal?
	{
		guard age - lastBreedAge >= breedTime else { return nil }

		lastBreedAge = age
		return self.clone(newAge: Bool.random(using: &rng) ? 0 : 1)
	}


	/// Make a new copy of self
	///
	/// This method must be overridden in subclasses.
	/// - Parameter newAge: The age of the new offspring.
	/// - Returns: A clone of self
	func clone(newAge: Int = 0) -> Animal
	{
		mustBeOverridden(self)
	}


	/// Move an animal from one place to another.
	///
	/// The default behaviour if not overridden is to stay put.
	///
	/// - Parameters:
	///   - from: The location the organism starts at
	///   - world: The world in which the animal moves.
	func move(from: World.Point, world: inout World)
	{
		// Default animal just sits in one place
	}
}


/// Models a fish
///
/// Fish just move around at random and breed.
class Fish: Animal
{

	/// Create a new fish
	///
	/// - Parameters:
	///   - breed: Age at wich the fish will breed
	///   - age: The current age of the fish.
	override init(breed: Int, age: Int = 0)
	{
		super.init(breed: breed, age: age)
	}


	/// Clone a new fish.
	///
	/// - Parameter newAge: The age of the clone - defaults to 0
	/// - Returns: A new fish.
	override func clone(newAge: Int = 0) -> Animal
	{
		return Fish(breed: self.breedTime, age: newAge)
	}


	/// Move the fish.
	///
	/// Fish move around randomly and breed when bnecessary.
	///
	/// - Parameters:
	///   - location: The location the fish is at
	///   - world: The world in which the fish is.
	override func move(from location: World.Point, world: inout World)
	{
		let freeLocations = world.findFreeLocationsAdjacent(to: location)
		if let newLocation = freeLocations.randomElement(using: &world.rng)
		{
			world[newLocation] = self
			if let baby = tryBreeding(rng: &world.rng)
			{
				world[location] = baby
			}
			else
			{
				world[location] = nil
			}
		}
	}
}


/// Models a shark
class Shark: Animal
{
	private let starveTime: Int
	private var lastAte: Int


	/// Create a new shark
	///
	/// - Parameters:
	///   - breed: Age at which the shark will breed.
	///   - starve: How long after eating a shark will starve
	///   - age: Age at birth.
	init(breed: Int, starve: Int, age: Int = 0)
	{
		self.starveTime = starve
		self.lastAte = age
		super.init(breed: breed, age: age)
	}


	/// Tells us if a shark should starve. i.e. it is too long since it last ate.
	var shouldStarve: Bool { return age - lastAte > starveTime }


	/// Create a baby shark the same as this,
	///
	/// - Parameter newAge: The age of the baby.
	/// - Returns: A baby shark
	override func clone(newAge: Int = 0) -> Animal
	{
		return Shark(breed: breedTime, starve: starveTime, age: newAge)
	}


	/// Moves and breeds the shark
	///
	/// Sharks first loo to see if any of the adsjacent locations contain a fish.
	/// If they find one, they move to that locvation and eat the fish. If not,
	/// they move to an empty location at random.
	///
	/// After moving, they trey to breed.
	/// - Parameters:
	///   - location: The location the shark is at.
	///   - world: The world it is in.
	override func move(from location: World.Point, world: inout World)
	{
		let adjacentLocations = location.adjacentPoints
		let fishLocations = adjacentLocations.filter { world[$0] is Fish }
		let moved: Bool
		if let eatLocation = fishLocations.randomElement(using: &world.rng)
		{
			world[eatLocation] = self
			lastAte = age
			moved = true
		}
		else
		{
			let freeLocations = adjacentLocations.filter{ world[$0] == nil }
			if let moveLocation = freeLocations.randomElement(using: &world.rng)
			{
				world[moveLocation] = self
				moved = true
			}
			else
			{
				moved = false
			}
		}
		if moved
		{
			if let baby = tryBreeding(rng: &world.rng)
			{
				world[location] = baby
			}
			else
			{
				world[location] = nil
			}
		}
	}
}
