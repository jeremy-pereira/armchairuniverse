//
//  World.swift
//  WaTor
//
//  Created by Jeremy Pereira on 31/08/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import ArmchairUniverseLib

/// A toroidal world of a given size
struct World
{
	/// Unit of time in the world
	typealias Chronon = Int
	/// The surface of the world
	private(set) var surface: [[Animal?]]

	var rng: AnyRNG
	private let bounds: Box
	/// The age of the world in chronons
	private(set) var age: Int = 0


	/// Number of points in the World
	var pointCount: Int { return bounds.pointCount }

	/// The size of the world
	var size: Vector { return bounds.size }

	/// Initialise the map.
	///
	/// - Parameters:
	///   - xBound: length of equator
	///   - yBound: length of a meridian
	init(xBound: Int, yBound: Int, rng: RandomNumberGenerator)
	{
		self.bounds = Box(bottomLeft: Point(0, 0), topRight: Point(xBound, yBound))
		self.rng = AnyRNG.wrap(rng)
		// This works because I am populating the whole world with nils. If I
		// used an instance of an Animal, I'd get the same animal everywhere
		let surfaceRow = Array<Animal?>(repeating: nil, count: xBound)
		surface = Array<[Animal?]>(repeating: surfaceRow, count: yBound)
	}


	/// The animal, if any at a particular point.
	///
	/// The point's coordinates are normailised by taking the modulus with
	/// respect to the dimensions of the world.
	///
	/// - Parameter point: The point to look for an animal
	subscript(point: Point) -> Animal?
	{
		get
		{
			let coordinate = bounds.normalise(point: point)
			return surface[coordinate.y][coordinate.x]
		}

		set
		{
			let coordinate = bounds.normalise(point: point)
			surface[coordinate.y][coordinate.x] = newValue
		}
	}


	/// Populate the world randomly with fish and sharks
	///
	/// - Parameters:
	///   - fishCount: number of fish in the world
	///   - sharkCount: number of sharks in the world
	///   - fishBreed: How long befor w
	///   - sharkBreed: Time interval between breeding
	///   - sharkStarve: How long a shark can go without food before starving
	mutating func populate(fishCount: Int, sharkCount: Int, fishBreed: Chronon, sharkBreed: Chronon, sharkStarve: Chronon) throws
	{
		guard fishCount + sharkCount < pointCount else { throw Error.tooManyFishInTheSea }
		// Put the fish in
		for _ in 0 ..< fishCount
		{
			let location = findEmptyLocation()
			self[location] = Fish(breed: fishBreed, age: (0 ..< fishBreed).randomElement(using: &rng)!)
		}
		// Put the sharks in
		for _ in 0 ..< sharkCount
		{
			let location = findEmptyLocation()
			self[location] = Shark(breed: fishBreed, starve: sharkStarve)
		}
	}

	private mutating func findEmptyLocation() -> Point
	{
		var ret: Point?
		while ret == nil
		{
			let candidate = bounds.randomPoint(rng: &rng)
			if self[candidate] == nil
			{
				ret = candidate
			}
		}
		return ret!
	}


	/// Move the World's time on by a number of chronons
	///
	/// - Parameter chronons: Chronons to step forward
	mutating func step(chronons: Int)
	{
		for _ in 0 ..< chronons
		{
			fishSwimAndBreed()
			sharksHuntAndBreed()
		}
		age += chronons
	}

	private mutating func fishSwimAndBreed()
	{
		// The followqing gets a tuple of the location of each fish and the fish
		// shuffled so we move the fish in a random order.
		let fishLocations = bounds.compactMap { self[$0] is Fish ? ($0, self[$0] as! Fish) : nil }.shuffled(using: &rng)

		for (location, fish) in fishLocations
		{
			fish.increaseAge()
			fish.move(from: location, world: &self)
		}
	}


	/// Convenience function to find empty locations adjacent to a point
	///
	/// - Parameter location: Loc ation to find adjacent empty locatiosn
	/// - Returns: A list of coordinates of empty locations.
	func findFreeLocationsAdjacent(to location: Point) -> [Point]
	{
		let adjacentLocations = location.adjacentPoints
		let ret = adjacentLocations.filter { self[$0] == nil }
		return ret
	}

	private mutating func sharksHuntAndBreed()
	{
		// The followqing gets a tuple of the location of each shark and the shark
		// shuffled so we move the sharks in a random order.
		let sharkLocations = bounds.compactMap { self[$0] is Shark ? ($0, self[$0] as! Shark) : nil }.shuffled(using: &rng)

		for (location, shark) in sharkLocations
		{
			shark.increaseAge()
			if shark.shouldStarve
			{
				self[location] = nil
			}
			else
			{
				shark.move(from: location, world: &self)
			}
		}
	}
}

extension World
{
	/// Represents a point on the surface of the world
	struct Point
	{
		let x: Int
		let y: Int

		init(_ x: Int, _ y: Int)
		{
			self.x = x
			self.y = y
		}

		/// Get a random point between bounds
		///
		/// - Parameters:
		///   - from: Lower left point
		///   - to: upper right point
		///   - rng: The random number generator to use
		/// - Returns: A random point
		static func random(from: Point, to: Point, rng: inout AnyRNG) -> Point
		{
			let x = (from.x ..< to.x).randomElement(using: &rng)!
			let y = (from.y ..< to.y).randomElement(using: &rng)!
			return Point(x, y)
		}

		private static let adjacentVectors = [Vector(0, 1), Vector(0, -1), Vector(1, 0), Vector(-1, 0)]

		var adjacentPoints: [Point]
		{
			return Point.adjacentVectors.map({ self + $0 })
		}
	}

	/// Represents a vector from one point to another.
	struct Vector
	{
		/// The x component of the vector
		let x: Int
		/// The y component of the vector
		let y: Int

		init(_ x: Int, _ y: Int)
		{
			self.x = x
			self.y = y
		}
	}

	/// Represents a rectangular box
	struct Box
	{
		/// The bottom left point of the box
		let bottomLeft: Point
		/// The top right of the box. Note that points on on the top and right
		/// edges are not considered to be in the box.
		let topRight: Point


		/// The size of the box as a vector.
		var size: Vector { return topRight - bottomLeft }


		/// Gives us a random point in  the box.
		///
		/// - Parameter rng: The random number generator to be used.
		/// - Returns: A random point in the box.
		func randomPoint(rng: inout AnyRNG) -> Point
		{
			let x = (bottomLeft.x ..< topRight.x).randomElement(using: &rng)!
			let y = (bottomLeft.y ..< topRight.y).randomElement(using: &rng)!
			return Point(x, y)
		}


		/// Normalises a point, so that it is in the box.
		///
		/// Modular arithmetic on the x and y coordinates is used to transform
		/// points outside the box to points inside the box.
		///
		/// - Parameter point: The ppint to normailse
		/// - Returns: The normalised point.
		func normalise(point: Point) -> Point
		{
			let originVector = bottomLeft - Point(0, 0)
			let zeroBasedPoint = point - originVector
			let newX = zeroBasedPoint.x.modulus(size.x)
			let newY = zeroBasedPoint.y.modulus(size.y)
			return Point(newX, newY) + originVector
		}

		/// The number of points in the box.
		var pointCount: Int
		{
			return size.x * size.y
		}
	}
}

extension World.Box: Sequence
{

	/// Makes `World.Box a sequence of points.
	///
	///
	/// - Returns: An iterator over all the points in the box.
	func makeIterator() -> World.Box.Iterator
	{
		return Iterator(box: self)
	}

	/// A box iterator
	struct Iterator: IteratorProtocol
	{
		private let box: World.Box
		private var x: Int
		private var y: Int

		fileprivate init(box: World.Box)
		{
			self.box = box
			self.x = box.bottomLeft.x
			self.y = box.bottomLeft.y
		}

		typealias Element = World.Point


		/// Gets the next poit in the sequence or nil if we have iterated over
		/// all the points.
		///
		/// - Returns: The next point in the sequence.
		mutating func next() -> World.Point?
		{
			guard y < box.topRight.y else { return nil }

			let ret = World.Point(x, y)
			x += 1
			if x >= box.topRight.x
			{
				x = box.bottomLeft.x
				y += 1
			}
			return ret
		}
	}
}

extension World
{
	/// Errors that can be generated by the World
	///
	/// - tooManyFishInTheSea: If we want to populate the Word with more sharks
	///                        and fish than are available
	enum Error: Swift.Error
	{
		case tooManyFishInTheSea
	}
}

extension World: Sequence
{
	typealias Element = Animal?

	/// A world iterator
	struct Iterator: IteratorProtocol
	{

		typealias Element = Animal?

		private var boundsIterator: World.Box.Iterator
		private var world: World

		fileprivate init(world: World)
		{
			self.world = world
			self.boundsIterator = world.bounds.makeIterator()
		}


		/// Get the next animal or empty cell.
		///
		/// - Returns: An animal or nil if the point is unpopulated,
		mutating func next() -> Optional<Animal>?
		{
			guard let point = boundsIterator.next() else { return nil }
			return world[point]
		}
	}


	/// Turns the world into a sequence of the things in it.
	///
	/// The elements are either animals or `nil` for empty points.
	///
	/// - Returns: An animal or `nil` if the point is empty.
	func makeIterator() -> World.Iterator
	{
		return Iterator(world: self)
	}
}


/// Gives us the vector from one poit to another.
///
/// - Parameters:
///   - lhs: One point
///   - rhs: another point
/// - Returns: The vector between the two points.
func - (lhs: World.Point, rhs: World.Point) -> World.Vector
{
	return World.Vector(lhs.x - rhs.x, lhs.y - rhs.y)
}


/// Subtracts a vector from a point to get another point
///
/// - Parameters:
///   - lhs: The initial pooitn
///   - rhs: The vector
/// - Returns: The point arrived at by subtrracting the vector
func - (lhs: World.Point, rhs: World.Vector) -> World.Point
{
	return World.Point(lhs.x - rhs.x, lhs.y - rhs.y)
}


/// Adds a vector to a point to get a new point.
///
/// - Parameters:
///   - lhs: A point
///   - rhs: A vector
/// - Returns: The point arrived at by adding the vector to the initial point.
func + (lhs: World.Point, rhs: World.Vector) -> World.Point
{
	return World.Point(lhs.x + rhs.x, lhs.y + rhs.y)
}


