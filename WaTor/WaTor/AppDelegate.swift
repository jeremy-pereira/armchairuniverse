//
//  AppDelegate.swift
//  WaTor
//
//  Created by Jeremy Pereira on 31/08/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{

	@IBOutlet weak var window: NSWindow!
	@IBOutlet weak var watorView: WatorView?

	@IBOutlet weak var fishCount: NSTextField!
	@IBOutlet weak var sharkCount: NSTextField!
	@IBOutlet weak var time: NSTextField!

	@IBOutlet var settingsController: SettingsController!

	private(set) var initialFishCount = 300
	private(set) var initialSharkCount = 200
	private(set) var fishBreedingAge = 12
	private(set) var sharkBreedingAge = 13
	private(set) var sharkStarvationTime = 8
	private(set) var worldSize = World.Vector(150, 150)

	var chrononTimer: Timer?

	var world: World?
	{
		didSet
		{
			if let wv = watorView
			{
				wv.dataSource = self
			}
			doStatistics()
		}
	}

	private func doStatistics()
	{
		if let world = world
		{
			let (fishCount, sharkCount) = world.reduce((0, 0))
			{
				let (f, s) = $0
				if $1 is Fish
				{
					return (f + 1, s)
				}
				else if $1 is Shark
				{
					return (f, s + 1)
				}
				else
				{
					return (f, s)
				}
			}
			self.fishCount.integerValue = fishCount
			self.sharkCount.integerValue = sharkCount
			time.integerValue = world.age
		}
		else
		{
			fishCount.integerValue = 0
			sharkCount.integerValue = 0
			time.integerValue = 0
		}

	}

	func applicationDidFinishLaunching(_ aNotification: Notification)
	{
		createNewWorld()
	}

	func applicationWillTerminate(_ aNotification: Notification)
	{
		// Insert code here to tear down your application
	}

	func createNewWorld()
	{
		var world = World(xBound: worldSize.x, yBound: worldSize.y, rng: SystemRandomNumberGenerator())
		do
		{
			try world.populate(fishCount: initialFishCount,
							   sharkCount: initialSharkCount,
							   fishBreed: fishBreedingAge,
							   sharkBreed: sharkBreedingAge,
							   sharkStarve: sharkStarvationTime)
			self.world = world
		}
		catch
		{
			fatalError("\(error)")
		}
	}


	@IBAction func stepOneChronon(sender: Any)
	{
		self.world?.step(chronons: 1)
		watorView?.needsDisplay = true
	}


	@IBAction func start(sender: Any)
	{
		guard chrononTimer == nil else { return }
		chrononTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block:
			{
				(_) in
				self.stepOneChronon(sender: self)
			})
	}

	@IBAction func stop(sender: Any)
	{
		guard let timer = chrononTimer else { return }
		timer.invalidate()
		chrononTimer = nil
	}

	@IBAction func reset(sender: Any)
	{
		stop(sender: self)
		settingsController.makeWorld(sheetWindow: self.window, worldOwner: self)
	}

}

extension AppDelegate: WatorViewDataSource
{
	func worldSize(for view: WatorView) -> World.Vector
	{
		guard let world = world else { return World.Vector(1, 1) }

		return world.size
	}

	func animal(forView: WatorView, at point: World.Point) -> Animal?
	{
		guard let world = world else { return nil }
		return world[point]
	}

}

extension AppDelegate: WorldOwner
{

	func reset(size: World.Vector,
			   fishCount: Int,
			   sharkCount: Int,
			   fishBreedingAge: Int,
			   sharkBreedingAge: Int,
			   sharkStarvationTime: Int)
	{
		worldSize = size
		initialFishCount = fishCount
		initialSharkCount = sharkCount
		self.fishBreedingAge = fishBreedingAge
		self.sharkBreedingAge = sharkBreedingAge
		self.sharkStarvationTime = sharkStarvationTime
		createNewWorld()
	}


}
