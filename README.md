# This repository is obsolete

Use wator, armchairuniverselib etc

# Armchair Universe 

This is a suite of programs used to implement the programs described in *The Armchair Universe* written by AK Dewdney.

The repositoery is constructed as an Xcode workspace (currently Xcode 10.3). There are no external dependencies at this time and, since the programs in the book are generally pretty simple, it will probably remain so forever.

## Structure

Each chapter gets its own project. Within a project, the parts not specifically related to the user interface are kept within a `model` group. 

There is a separate `ArmchairUniverseLib` which contains code that might be handy in more than one project.

## Common Code
`ArmchairUniverseLib` contains some utility classes and functions.


## WaTor

This program is designed to simulate the sharks and fish world of section 6. 

See https://sincereflattery.blog/2019/09/10/sharks-and-fishes/
